package synapse.com.notificationsampleapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button buttonCreateNotification;
    Button buttonCreateDelayedNotification;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonCreateNotification = findViewById(R.id.button_create_notification);

        buttonCreateDelayedNotification = findViewById(R.id.button_create_delayed_notification);

        context = this;


        if(!isNotificationAccessGranted()){
            redirectToNotificationAccessScreen();
        }


        buttonCreateNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                createSampleNotification(context);
            }
        });

        buttonCreateDelayedNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                createSampleNotification(context);
            }
        });

    }

    private boolean isNotificationAccessGranted() {
        ContentResolver contentResolver = this.getContentResolver();
        String enabledNotificationListeners = Settings.Secure.getString(contentResolver, "enabled_notification_listeners");
        String packageName = this.getPackageName();

        if (enabledNotificationListeners == null || !enabledNotificationListeners.contains(packageName))
            return false;
        else
            return true;
    }

    private void redirectToNotificationAccessScreen() {
        startActivity(new Intent("android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS"));
        finish();
    }

    private void createSampleNotification(Context context) {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(100, generateDefaultNotificationBuilder(context).build());

    }


    public static Notification.Builder generateDefaultNotificationBuilder(Context context){

        NotificationUtils notificationUtils = new NotificationUtils(context);
        Notification.Builder builder = notificationUtils.getNotificationBuilder(context);

        Intent intent = new Intent(context, MainActivity.class);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 1000,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);



        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Sample title")
                .setContentText("Sample text")
                .setAutoCancel(true)
                .setOngoing(false)
                .setContentIntent(contentIntent)
                .setTicker("Sample ticker")
                .setVibrate(new long[] { 0, 1000, 100, 1000})
                .setPriority(Notification.PRIORITY_MAX);



        return builder;


    }


}
