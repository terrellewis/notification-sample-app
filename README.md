*Background: As you know, our app Daywise batches notifications to reduce interruptions people get during certain hours of the day.*

**Overview of working:**

**1. Notification listener service is used to listen for incoming notifications**

**2. Incoming notifications are captured in the *onNotificationPosted()* method**

**3. Each notification is processed to determine whether it has to be batched or not**

**4. If it's supposed to be batched, the notification is hidden using the *cancelNotification()* method**

**5. Because there's a slight delay between when a notification comes in and when it gets cancelled, the vibration associated with that notification comes into effect partially and the screen wakes up**

*Steps to reproduce the problem using the sample app:*

**1. Install the APK and launch the app**

**2. You will be required to grant Notification Access to *Notification Sample App* **

**3. Once you give access, you will be redirected to the app**

**4. Upon tapping the first button, a notification is created by the app which is then cancelled by the Notification Service. A 50ms delay is introduced prior to cancelling the notification to mimic the processing time from the actual app. A partial buzz can be experienced as a result of this.**

**5. In order to reproduce the screen wake problem, the second button should be tapped and the phone screen should be turned off. A notification will be created after 5 seconds which is then subsequently cancelled by the Notification Service. As a result, the screen wakes up but there is no notification to see.**


*Code related to listening for and cancelling notifications can be found in **NotificationService.class** and code related to creating sample notifications can be found in **MainActivity.class***

